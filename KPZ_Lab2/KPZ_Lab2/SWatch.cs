﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace KPZ_Lab2
{
    public class SWatch
    {
        public event UpdateHandler Update;

        protected virtual void onUpdate(UpdateEventArgs e)
        {
            UpdateHandler update = Update;
            if (update != null)
                update(this, e);
        }

        public class UpdateEventArgs : EventArgs
        {
            public int minutes { get; set; }
            public int seconds { get; set; }
        }

        public void start()
        {
            DateTime start = DateTime.Now;
            var sw = Stopwatch.StartNew();
            double seconds, minutes = 0.0;
            do
            {
                Thread.Sleep(500);
                seconds = sw.ElapsedMilliseconds / 1000.0;
                if (seconds >= 60)
                {
                    minutes = (sw.ElapsedMilliseconds / 1000.0) / 60.0;
                    seconds -= 60.0;
                }
                UpdateEventArgs args = new UpdateEventArgs();
                args.minutes = (int)minutes;
                args.seconds = (int)seconds;
                onUpdate(args);
            } while (sw.ElapsedMilliseconds <= 5.0 * 60.0 * 1000.0);
        }
    }

    public delegate void UpdateHandler(object sender, SWatch.UpdateEventArgs e);

}
