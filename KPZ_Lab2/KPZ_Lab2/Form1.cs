﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace KPZ_Lab2
{
    public partial class Form1 : Form
    {
        public delegate void UpdateHandler(object sender, SWatch.UpdateEventArgs e);

        public Form1()
        {
            InitializeComponent();
        }

        private void Update(object sender, SWatch.UpdateEventArgs e)
        {
            if (labelMinutes.InvokeRequired)
            {
                UpdateHandler d = new UpdateHandler(Update);
                Invoke(d, new object[] { sender, e });

            }
            else
            {
                labelMinutes.Text = "0" + e.minutes;
                if (e.seconds >= 10) labelSeconds.Text = "" + e.seconds;
                else labelSeconds.Text = "0" + e.seconds;
            }
        }

        private void Start_Click(object sender, EventArgs e)
        {
            var sWatch = new SWatch();
            var thread = new Thread(sWatch.start);
            thread.Start();
            sWatch.Update += Update;
        }

        private void Stop_Click(object sender, EventArgs e)
        {
            
        }
    }
}
