﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Robot.Common;
using System.Collections.Generic;

namespace Kalyniak.Andrii.RobotChallenge.Test
{
    [TestClass]
    public class TestAlgorithm
    {
        [TestMethod]
        public void TestRecharge()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            Map map = new Map();
            Owner owner = new Owner() { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(0, 0)
            });

            Assert.IsTrue(algorithm.DoStep(robots, 0, map) is CollectEnergyCommand);
        }

        [TestMethod]
        public void TestMove()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            Map map = new Map();
            Owner owner = new Owner() { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(10, 0)
            });

            Assert.IsTrue(algorithm.DoStep(robots, 0, map) is MoveCommand);
        }

        [TestMethod]
        public void TestEnoughRobots()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            for (int i = 0; i < 35; i++)
            {
                robots.Add(new Robot.Common.Robot()
                {
                    Position = new Position(i, i),
                    Energy = 10,
                    Owner = owner
                });
            }
            for (int i = 0; i < 40; i++)
            {
                map.Stations.Add(new EnergyStation()
                {
                    Position = new Position(i + 2, i),
                    RecoveryRate = 10,
                    Energy = 100
                });
            }
            Assert.AreEqual(false, algorithm.isEnoughRobots(robots.Count, map));

            for (int i = 0; i < 5; i++)
            {
                robots.Add(new Robot.Common.Robot()
                {
                    Position = new Position(35 + i, 35 + i),
                    Energy = 10,
                    Owner = owner
                });
            }
            Assert.AreEqual(true, algorithm.isEnoughRobots(robots.Count, map));
        }

        [TestMethod]
        public void TestOnStation()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(10, 0)
            });
            Assert.AreEqual(false, algorithm.isOnStation(robots[0], map));


            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(0, 0)
            });
            Assert.AreEqual(true, algorithm.isOnStation(robots[0], map));
        }

        [TestMethod]
        public void TestGetDistance()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(10, 0)
            });
            Assert.AreEqual(10, algorithm.getDistance(robots[0].Position, map.Stations[0].Position));                
        }

        [TestMethod]
        public void TestGetClosestStation()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(20, 0)
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(10, 0)
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(30, 0)
            });
            Position testPosition = new Position(10, 0);
            Assert.AreEqual(testPosition, algorithm.getClosestStation(robots[0], map, robots));
        }

        [TestMethod]
        public void TestEnoughEnergy()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(20, 0)
            });
            Assert.AreEqual(false, algorithm.isEnoughEnergy(robots[0], map.Stations[0].Position));

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(10, 0)
            });
            Assert.AreEqual(true, algorithm.isEnoughEnergy(robots[0], map.Stations[1].Position));
        }

        [TestMethod]
        public void TestGetMiddle()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(20, 0)
            });
            Position testPosition = new Position(10, 0);
            Assert.AreEqual(testPosition, algorithm.getMiddle(robots[0].Position, map.Stations[0].Position));
        }

        [TestMethod]
        public void TestEnoughCreationEnergy()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 100,
                Owner = owner
            });

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(0, 0),
                Energy = 500,
                Owner = owner
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(20, 0)
            });
            Assert.AreEqual(false, algorithm.isEnoughCreationEnergy(robots[0]));
            Assert.AreEqual(true, algorithm.isEnoughCreationEnergy(robots[1]));
        }

        [TestMethod]
        public void TestIsClosest()
        {
            var algorithm = new KalyniakAndriiRobotAlgorithm();
            var map = new Map();
            Owner owner = new Owner { Algorithm = algorithm, Name = "MyName" };
            Owner rival = new Owner { Algorithm = algorithm, Name = "NotMyName" };
            List<Robot.Common.Robot> robots = new List<Robot.Common.Robot>();

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(10, 0),
                Energy = 100,
                Owner = owner
            });

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(5, 0),
                Energy = 100,
                Owner = owner
            });

            robots.Add(new Robot.Common.Robot()
            {
                Position = new Position(15, 0),
                Energy = 100,
                Owner = rival
            });

            map.Stations.Add(new EnergyStation()
            {
                RecoveryRate = 10,
                Energy = 100,
                Position = new Position(20, 0)
            });
            Assert.AreEqual(true, algorithm.isClosestToStaton(robots, robots[0], map.Stations[0]));
        }


    }
}
