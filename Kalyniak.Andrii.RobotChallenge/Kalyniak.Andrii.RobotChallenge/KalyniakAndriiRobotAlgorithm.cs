﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Robot.Common;

namespace Kalyniak.Andrii.RobotChallenge
{
    public class KalyniakAndriiRobotAlgorithm : IRobotAlgorithm
    {
        int RoundCount = 0;
        public KalyniakAndriiRobotAlgorithm()
        {
            Logger.OnLogRound += Logger_OnLogRound;
        }

        private void Logger_OnLogRound(object sender, LogRoundEventArgs e)
        {
            RoundCount++;
        }

        public List<Position> takenList = new List<Position>();
        public string Author
        {
            get
            {
                return "Kalyniak Andrii";
            }
        }

        public string Description
        {
            get
            {
                return "Lorem ipsum";
            }
        }

        public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
        {
            var myRobot = robots[robotToMoveIndex];

            var newposition = myRobot.Position;
            foreach (EnergyStation station in map.Stations)
                if (station.Position.Equals(myRobot.Position))
                    return new CollectEnergyCommand();
            if (!isOnStation(myRobot, map))
            {
                Position closestStation = getClosestStation(myRobot, map, robots);
                newposition = closestStation;
                if (getDistanceClass(myRobot, newposition) == 3)
                {
                    while (!isEnoughEnergy(myRobot, newposition))
                    {
                        newposition = getMiddle(myRobot.Position, newposition);
                    }
                }
                else if (getDistanceClass(myRobot, newposition) == 2)
                {
                    newposition.X = Convert.ToInt32(closestStation.X * 0.6);
                    newposition.Y = Convert.ToInt32(closestStation.Y * 0.6);
                }
                else if (getDistanceClass(myRobot, newposition) == 1)
                {
                    newposition.X = Convert.ToInt32(closestStation.X * 0.3);
                    newposition.Y = Convert.ToInt32(closestStation.Y * 0.3);
                }
                else if (getDistanceClass(myRobot, newposition) == 0)
                {
                    newposition.X = Convert.ToInt32(closestStation.X * 0.1);
                    newposition.Y = Convert.ToInt32(closestStation.Y * 0.1);
                }
            }
            else if (isOnStation(myRobot, map) && isEnoughCreationEnergy(myRobot) && RoundCount <= 30)
                return new CreateNewRobotCommand();
            
            return new MoveCommand() { NewPosition = newposition };
        }

        public bool isEnoughRobots(int robots, Map map)
        {
            if (robots >= map.Stations.Count)return true;
                else return false;
        }

        public bool isOnStation(Robot.Common.Robot robot, Map map)
        {
            var position = robot.Position;
            bool onStation = false;
            foreach (EnergyStation station in map.Stations)
                if (station.Position.Equals(position))
                    onStation = true;
            if (onStation) return true;
            else return false;
        }

        public bool isClosestToStaton(IList<Robot.Common.Robot> robots, Robot.Common.Robot robot, EnergyStation station)
        {
            bool isClosest = true;
            foreach (Robot.Common.Robot tempRobot in robots)
            {
                if (getDistance(tempRobot.Position, station.Position) < getDistance(robot.Position, station.Position) && tempRobot.Owner == robot.Owner) isClosest = false;
            }
            return isClosest;
        }

        public double getDistance(Position position1, Position position2)
        {
            return Math.Sqrt(Math.Pow(position1.X - position2.X, 2) + Math.Pow(position1.Y - position2.Y, 2));
        }

        public Position getClosestStation(Robot.Common.Robot robot, Map map, IList<Robot.Common.Robot> robots)
        {
            Position closestStationPosition = new Position(0, 0);
            double closestStationDistance = 99999;
            foreach (EnergyStation station in map.Stations)
            {
                bool isTaken = false;
                foreach (Position taken in takenList)
                    if (station.Position == taken) isTaken = true;
                if (!isTaken && getDistance(robot.Position, station.Position) < closestStationDistance && isClosestToStaton(robots, robot, station))
                {
                    closestStationDistance = getDistance(robot.Position, station.Position);
                    closestStationPosition = station.Position;
                }
            }
            takenList.Add(closestStationPosition);
            return closestStationPosition;
        }

        public bool isEnoughEnergy(Robot.Common.Robot robot, Position moveTo)
        {
            double distance = getDistance(robot.Position, moveTo);
            if (distance > Math.Sqrt(robot.Energy)) return false;
            else return true;
        }

        public bool isEnoughCreationEnergy(Robot.Common.Robot robot)
        {
            if (robot.Energy < 400) return false;
            else return true;
        }

        public Position getMiddle(Position A, Position B)
        {
            Position middle = new Position((A.X + B.X) / 2, (A.Y + B.Y) / 2);
            return middle; 
        }

        public int getDistanceClass(Robot.Common.Robot robot, Position position)
        {
            double distance = getDistance(robot.Position, position);
            if (distance > 50) return 0;
            else if (distance > 20) return 1;
            else if (distance > 10) return 2;
            else return 3;
        }


    }
}
