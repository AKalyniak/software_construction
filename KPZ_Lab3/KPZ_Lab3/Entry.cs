﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
    class Entry
    {
        public int ID { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public List<string> Tags = new List<string>();
        public bool Approved;

        public Entry(int id, string name, string description, List<string> tags, bool approved)
        {
            ID = id;
            Name = name;
            Description = description;
            Tags = tags;
            Approved = approved;
        }

        public override string ToString()
        {
            return $"{ID} \t|\t {Name} \t|\t {Approved}";
        }
    }
}
