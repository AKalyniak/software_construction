﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
    class TagCountComparer : IComparer<IGrouping<int, Entry>>
    {
        public int Compare(IGrouping<int, Entry> x, IGrouping<int, Entry> y)
        {
            if (x.Key > y.Key) return 1;
            if (x.Key < y.Key) return -1;
            return 0;
        }
    }
}
