﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
    static class EntryExtension
    {
        static public int getTagCount(this Entry entry)
        {
            return entry.Tags.Count;
        }
    }
}
