﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.OutputEncoding = Encoding.UTF8;

            var entries = new List<Entry>();
            for (int i = 0; i < 10; i++)
            {
                var tags = new List<string>()
                {
                    "Tag" + i%2,
                };
                for (int j = 0; j < (i % 2) * i; j++)
                {
                    tags.Add("Tag" + i + j);
                }
                bool approved;
                var random = new Random(2);
                if (random.NextDouble() >= 0.5) approved = true;
                else approved = false;
                entries.Add(new Entry(i, "Entry_" + (i + 1), "Description_" + (i + 1), tags, approved));
            }

            //Select, anonymous classes
            var listSelect = entries.Select(entry => new
            {
                Name = entry.Name,
                TagCount = entry.getTagCount()      //extension method
            });
            Console.WriteLine("All entries:");
            Console.WriteLine("\t Name \t       Tag count");
            foreach (var item in listSelect)
                Console.WriteLine($"\t {item.Name} \t {item.TagCount}");
            Console.WriteLine();

            //Sorting
            listSelect.OrderByDescending(entry => entry.TagCount).ToList();
            Console.WriteLine("All entries (Sorted by tag count):");
            Console.WriteLine("\t Name \t       Tag count");
            foreach (var item in listSelect)
                Console.WriteLine($"\t {item.Name} \t {item.TagCount}");
            Console.WriteLine();

            //information selection (Where)
            var listWhere = entries.Where(entry => entry.getTagCount() > 1).ToList();
            Console.WriteLine("Multiple tag entries:");
            Console.WriteLine("\tID\t|\t  Name   \t|\tApproved");
            foreach (var item in listWhere)
                Console.WriteLine("\t" + item.ToString());
            Console.WriteLine();

            //Dictionary
            var dictionary = new SortedDictionary<int, int>();
            foreach (var item in entries)
                dictionary.Add(item.ID, item.getTagCount());
            Console.WriteLine("Entries and corresponding tag counts:");
            Console.WriteLine("\tID\tTag count");
            foreach(var item in dictionary)
            {
                Console.WriteLine($"\t{item.Key}\t{item.Value}");
            }
            Console.WriteLine();

            //Grouping
            var listGroup = entries.GroupBy(entry => entry.getTagCount()).ToList();
            listGroup.Sort(new TagCountComparer()); //Sorting using the IComparer
            var arrayGroup = listGroup.ToArray(); //List to array convertion
            Console.WriteLine("Entries by tag count:");
            foreach(var tagEntry in arrayGroup)
            {
                Console.WriteLine($"Tag count = {tagEntry.Key}");
                foreach (var entry in tagEntry)
                    Console.WriteLine("\t" + entry.ToString());
            }

            Console.ReadKey();
        }
    }
}
